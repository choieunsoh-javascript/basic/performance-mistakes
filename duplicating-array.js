const count = 10000000;
const nums = new Array(count).fill(0).map((n, i) => i + 1);
let array;

console.log(`Duplication: ${Intl.NumberFormat().format(count)} items`);

console.time("slice");
array = nums.slice();
console.timeEnd("slice");

console.time("map");
array = nums.map((x) => x);
console.timeEnd("map");

console.time("spread");
array = [...nums];
console.timeEnd("spread");

console.time("concat");
array = [].concat(nums);
console.timeEnd("concat");

console.time("array-from");
array = Array.from(nums);
console.timeEnd("array-from");

console.time("for-loop");
for (let i = 0; i < nums.length; i++) {
  array[i] = nums[i];
}
console.timeEnd("for-loop");
