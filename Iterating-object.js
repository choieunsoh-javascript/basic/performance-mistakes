const count = 1000000;
const nums = {};
for (let i = 0; i < count; i++) {
  nums[i] = i;
}
let array = [];

console.log(`Iterating Object: ${Intl.NumberFormat().format(count)} keys`);

console.time("for-in");
array = [];
for (const key in nums) {
  array.push(nums[key]);
}
console.timeEnd("for-in");

console.time("for-loop object-keys");
array = [];
let keys = Object.keys(nums);
for (let i = 0; i < keys.length; i++) {
  array.push(nums[keys[i]]);
}
console.timeEnd("for-loop object-keys");

console.time("for-of object-keys");
array = [];
keys = Object.keys(nums);
for (const key of keys) {
  array.push(nums[key]);
}
console.timeEnd("for-of object-keys");

console.time("for-loop object-entries");
array = [];
let entries = Object.entries(nums);
for (let i = 0; i < entries.length; i++) {
  array.push(entries[i][1]);
}
console.timeEnd("for-loop object-entries");

console.time("for-of object-entries");
array = [];
entries = Object.entries(nums);
for (const entry of entries) {
  array.push(entry[1]);
}
console.timeEnd("for-of object-entries");
