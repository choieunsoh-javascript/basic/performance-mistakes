const count = 10000000;
const nums = new Array(count).fill(0).map((n, i) => i + 1);

console.log(`For Loop: ${Intl.NumberFormat().format(count)} items`);

console.time("for-loop");
let sum = 0;
for (let i = 0; i < count; i++) {
  sum += nums[i];
}
//console.log(sum);
console.timeEnd("for-loop");

console.time("for-of");
sum = 0;
for (const num of nums) {
  sum += num;
}
//console.log(sum);
console.timeEnd("for-of");

console.time("forEach");
sum = 0;
nums.forEach((num) => (sum += num));
//console.log(sum);
console.timeEnd("forEach");

console.time("while");
sum = 0;
let i = 0;
while (i < nums.length) {
  sum += nums[i];
  i++;
}
//console.log(sum);
console.timeEnd("while");

console.time("reduce");
sum = nums.reduce((s, n) => s + n, 0);
//console.log(sum);
console.timeEnd("reduce");
