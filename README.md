# 1. Looping Over an Array

```
# article
For Loop, average loop time: ~10 microseconds
For-Of, average loop time: ~110 microseconds
ForEach, average loop time: ~77 microseconds
While, average loop time: ~11 microseconds
Reduce, average loop time: ~113 microseconds

# me
For Loop: 10,000,000 items
for-loop: 83.642ms
for-of: 163.808ms
forEach: 145.673ms
while: 80.316ms
reduce: 105.895ms
```

# 2. Duplicating an Array

```
# article
Duplicate using Slice, average: ~367 microseconds
Duplicate using Map, average: ~469 microseconds
Duplicate using Spread, average: ~512 microseconds
Duplicate using Concat, average: ~366 microseconds
Duplicate using Array From, average: ~1,436 microseconds
Duplicate manually, average: ~412 microseconds

# me
Duplication: 10,000,000 items
slice: 26.949ms
map: 122ms
spread: 25.875ms
concat: 27.054ms
array-from: 27.36ms
for-loop: 13.977ms
```

# 3. Iterating Objects

```
# article
Object iterate For-In, average: ~240 microseconds
Object iterate Keys For Each, average: ~294 microseconds
Object iterate Entries For-Of, average: ~535 microseconds

# me
Iterating Object: 1,000,000 keys
for-in: 184.596ms
for-loop object-keys: 154.952ms
for-of object-keys: 159.892ms
for-loop object-entries: 468.012ms
for-of object-entries: 350.361ms
```
